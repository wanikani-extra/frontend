import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import axios from 'axios'
import { settingsStore } from '~/utils/store-accessor'

export interface IWkResource<T = any> {
  id: number
  integer: string
  url: string
  data_updated_at: string // Date
  data: T
}

export interface IWkCollection<T = any> {
  object: string
  url: string
  pages: {
    next_url?: string
    previous_url?: string
    per_page: number
  }
  total_count: number
  data_updated_at: string // Date
  data: T[]
}

export interface IWkError {
  error: string
  code: number
}

export interface Item {
  id: number
  srsLevel: number
}

@Module({
  name: 'wanikani',
  stateFactory: true,
  namespaced: true
})
export default class WaniKaniModule extends VuexModule {
  items: Item[] | null = null

  @Mutation
  setItems(items: Item[]) {
    this.items = items
  }

  @Action({ commit: 'setItems' })
  async doCache() {
    if (this.items) {
      return this.items
    }

    if (!settingsStore.apiKey) {
      return this.items || null
    }

    const wkApi = axios.create({
      baseURL: 'https://api.wanikani.com/v2/',
      headers: {
        Authorization: `Bearer ${settingsStore.apiKey}`
      }
    })

    let nextUrl = '/assignments'
    const allData: Item[] = []

    while (true) {
      const r = await wkApi.get<
        IWkCollection<
          IWkResource<{
            subject_id: number
            srs_stage: number
          }>
        >
      >(nextUrl, {
        params: {
          unlocked: 'true'
        }
      })

      r.data.data.map((d) => {
        allData.push({
          id: d.data.subject_id,
          srsLevel: d.data.srs_stage
        })
      })

      nextUrl = r.data.pages.next_url || ''
      if (!nextUrl) {
        break
      }
    }

    return allData
  }
}
