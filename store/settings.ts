import {
  Module,
  VuexModule,
  Mutation,
  MutationAction
} from 'vuex-module-decorators'
import { User } from 'firebase/app'
import { $axios } from '~/utils/api'

@Module({
  name: 'settings',
  stateFactory: true,
  namespaced: true
})
export default class SettingsModule extends VuexModule {
  user: User | null = null
  chineseMode = false
  minSrsLevel = 0
  maxSrsLevel = 9
  apiKey = ''
  isAuthInit = false

  @Mutation
  setChineseMode(isOn: boolean) {
    this.chineseMode = isOn
  }

  @Mutation
  setMinSrsLevel(lv: number) {
    this.minSrsLevel = lv
  }

  @Mutation
  setMaxSrsLevel(lv: number) {
    this.maxSrsLevel = lv
  }

  @Mutation
  setApiKey(key: string) {
    this.apiKey = key
  }

  @MutationAction({ mutate: ['user', 'isAuthInit', 'apiKey'] })
  async login(user: User | null) {
    const { apiKey } = await $axios.$post(
      '/api/login',
      JSON.parse(JSON.stringify(user))
    )

    return { user, isAuthInit: true, apiKey }
  }
}
