import Vue from 'vue'
import { VueContext } from 'vue-context'
import 'vue-context/src/sass/vue-context.scss'

Vue.component('vue-context', VueContext)
