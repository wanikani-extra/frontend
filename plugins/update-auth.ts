import { Plugin } from '@nuxt/types'
import { settingsStore } from '~/store'

const updateAuth: Plugin = ({ app }) => {
  if (process.client) {
    app.$fireAuth.onAuthStateChanged((user) => {
      settingsStore.login(user)
    })
  }
}

export default updateAuth
