import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import SettingsStore from '~/store/settings'
import WanikaniStore from '~/store/wanikani'

let settingsStore: SettingsStore
let wanikaniStore: WanikaniStore

function initialiseStores(store: Store<any>): void {
  settingsStore = getModule(SettingsStore, store)
  wanikaniStore = getModule(WanikaniStore, store)
}

export { initialiseStores, settingsStore, wanikaniStore }
